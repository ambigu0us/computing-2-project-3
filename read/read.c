#include <stdio.h>
#include <math.h>

int main() {

	FILE *data, *out; // specfy the file pointer
	

	int ch, id, lines=0,good=0,bad=0; // define ints
	float t1, t2, E, sumE=0, sumt2=0; //define floats

	data=fopen("p3data7b.dat","r"); // open the file to read
	if (data==0) { // if open fails quit and print error message
		printf("Cannot open data file.\n");
		return 1;
	}

	while ((ch=fgetc(data)) != EOF) { //If there is still characters left in the stream
		ungetc(ch, data);
		if ( ch == '#' ) { //check for header line or other comments
			while((ch=fgetc(data)) != '\n') {} // if so, go to end of that line
		} else { //This runs on each data line
			fscanf(data,"Event: %d t1 =  %f t2 =  %f %*s =  %f\n", &id, &t1, &t2, &E); // extract the data 
			lines++; //increment the data line counter
			//printf("%d %f %f\n",lines, E, t2);
			
			if (E > 0.0) { //check for positive E and if so, do maths!
				good++;
				sumE = sumE + E;
				sumt2 = sumt2 + t2;
				
			} else {
				bad++;
			}


		}
	}
	
	//quick sanity check
	if (good <= 0) {
		printf("No good events, exiting\n");	
		return 1;
	}


	out=fopen("py11twb-task1.dat","w"); // open the file to write
		if (data==0) { // if open fails quit and print error message
		printf("Cannot open output file, check permissions\n");
		return 1;
	}

	fprintf(out,"Good events:\t %d\nBad events:\t %d\nAverage energy:\t %f\nAverage T2:\t %f\n",good,bad,sumE/good,sumt2/good); //print results to file


	fclose(data); //close the data file
	fclose(out); //close the output file
	return 0; //exit normally
}
