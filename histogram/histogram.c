#include <stdio.h>
#include <math.h>
#define SEP 3 // separation of the sensors in m
#define C 299792458

double maxE=0,maxLife=0;

double beta(double t1, double t2) {
	double vel = SEP / ((t2 -t1) * pow(10,-9));
	return vel / C;
}

double gamma(double beta) {
	double gamma = pow(( 1 - (beta*beta)),-0.5);
	return gamma;
}

double lifeTransform(double t1, double t2) {
	double lifetime = t2 / gamma(beta(t1,t2));
	return lifetime;
}


double energyTransform(double t1, double t2, double E) {
	double electronRestmass = 0.510998910;
	double totalEnergy =  E + electronRestmass;// * 3 * pow(10,8);
	double momentum = pow((pow(totalEnergy,2) - pow(electronRestmass,2)),0.5);
	double transformedE = gamma(beta(t1,t2)) * (totalEnergy - momentum * beta(t1,t2));
	return transformedE;
	//return totalEnergy;
}

/*
int main() {
	double t1=91199.24779,t2=91209.25718,E=3087.557027;
	printf("%f\n", lifeTransform(t1,t2));
	return 0;

}
*/

void checkMaxE (double E) {
	if ( E > maxE ) {
		maxE = E;
	}
}

void checkMaxLife (double life) {
	if ( life > maxLife ) {
		maxLife = life;
	}
}

int main() {

	FILE *data, *out; // specfy the file pointer
	

	int ch, id, lines=0,good=0,bad=0; // define ints
	double t1, t2, E, sumE=0, sumt2=0, sumTransE=0, sumTransLife=0; //define floats

	data=fopen("p3data1M.dat","r"); // open the file to read
	if (data==0) { // if open fails quit and print error message
		printf("Cannot open data file.\n");
		return 1;
	}

	while ((ch=fgetc(data)) != EOF) { //If there is still characters left in the stream
		ungetc(ch, data);
		if ( ch == '#' ) { //check for header line or other comments
			while((ch=fgetc(data)) != '\n') {} // if so, go to end of that line
		} else { //This runs on each data line
			fscanf(data,"Event: %d t1 =  %lg t2 =  %lg %*s =  %lg\n", &id, &t1, &t2, &E); // extract the data 
			lines++; //increment the data line counter
			
			if (E > 0.0) { //check for positive E and if so, do maths!
				good++;
				//printf("Event: %d\t Transformed energy: %f\t Transformed lifetime: %f\n",lines, energyTransform(t1,t2,E), lifeTransform(t1,t2));
				sumE = sumE + E;
				sumt2 = sumt2 + t2;
				sumTransE = sumTransE + energyTransform(t1,t2,E);
				sumTransLife = sumTransLife + lifeTransform(t1,t2);
				checkMaxE(energyTransform(t1,t2,E));
				checkMaxLife(lifeTransform(t1,t2));
				
			} else {
				bad++;
			}


		}
	}
	
	//quick sanity check
	if (good <= 0) {
		printf("No good events, exiting\n");	
		return 1;
	}


	out=fopen("py11twb-task2.dat","w"); // open the file to write
		if (data==0) { // if open fails quit and print error message
		printf("Cannot open output file, check permissions\n");
		return 1;
	}

//	fprintf(out,"Good events:\t %d\nBad events:\t %d\nAverage energy:\t %f\nAverage T2:\t %f\n",good,bad,sumE/good,sumt2/good); //print results to file
	fprintf(out, "Good events:\t %d\nBad events:\t %d\nAverage energy:\t %f\nAverage T2:\t %f\nAverage Transformed Energy:\t %f\nMax Transformed Energy:\t %f\nAveraged Transformed Lifetime:\t %f\nMax Transformed Lifetime:\t %f\n",good,bad,sumE/good,sumt2/good,sumTransE/good,maxE,sumTransLife/good,maxLife); //print results to file


	fclose(data); //close the data file
	fclose(out); //close the output file
	return 0; //exit normally
}

